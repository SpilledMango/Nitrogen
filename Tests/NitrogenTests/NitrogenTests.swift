import Foundation
import XCTest
@testable import Nitrogen

final class NitrogenTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results. 
        print("Testing window creation.")

        let drawLoop: (NWindow) -> Void = {
            window in
            window.graphics.fillColor = NColor.red
            window.graphics.fill(NRect<Int>(x: 200, y: 200, width: 200, height: 200))
            window.graphics.strokeColor = NColor.green
            window.graphics.drawLine(from: NPoint<Int>(x: 0, y: 0), to: NPoint<Int>(x: 500, y: 500))
            window.graphics.fillColor = NColor.cyan
            window.graphics.fillElipse(in: NRect<Int>(x: 600, y: 400, width: 200, height: 200))
        }
        
        let window = try! NWindow(frame: NRect(x: 0, y: 0, width: 1080, height: 720), title: "Nitrogen", drawLoop: drawLoop)
        window.graphics.backgroundColor = NColor.yellow
        window.drawLoop()
        
        print("Testing event loop.")
        window.eventLoop()
        print("Done.")
    }


    static var allTests = [
        ("testExample", testExample),
    ]
}

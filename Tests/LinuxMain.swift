import XCTest

import NitrogenTests

var tests = [XCTestCaseEntry]()
tests += NitrogenTests.allTests()
XCTMain(tests)
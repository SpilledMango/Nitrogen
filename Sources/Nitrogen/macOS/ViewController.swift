import Cocoa

class ViewController: NSViewController {

    override func loadView() {
        let view = View(frame: NSRect(origin: NSPoint(x: 0, y: 0), size: AppDelegate.getAppDelegate().window!.frame.size))
        self.view = view
    }
}

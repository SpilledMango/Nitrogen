import Cocoa

class AppDelegate: NSObject, NSApplicationDelegate {
    var window: NSWindow?
    var controller: ViewController?
    var frame: NRect<Int>
    
    var nwindow: NWindow?
    
    init(window nwindow: NWindow) {
        self.frame = nwindow.frame
        self.nwindow = nwindow
    }
    func applicationWillFinishLaunching(_ notification: Notification) {
    }
    func applicationDidFinishLaunching(_ notification: Notification) {
        window = NSWindow(contentRect: NSRect(self.frame), styleMask: NSWindow.StyleMask(rawValue: NSWindow.StyleMask.titled.rawValue | NSWindow.StyleMask.closable.rawValue), backing: .buffered, defer: false)
        controller = ViewController()
        let content = window!.contentView! as NSView
        let view = controller!.view
        content.addSubview(view)
        
        window!.makeKeyAndOrderFront(nil)
    }
    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }
    static func getAppDelegate() -> AppDelegate {
        return NSApp.delegate as! AppDelegate
    }
}

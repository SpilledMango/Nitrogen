import Cocoa

class View: NSView {
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        if let nwindow = AppDelegate.getAppDelegate().nwindow {
            if let cgContext = NSGraphicsContext.current?.cgContext {
                cgContext.setFillColor(NSColor(nwindow.graphics.backgroundColor).cgColor)
                cgContext.fill(self.frame)
                nwindow.drawLoopBody(nwindow)
                cgContext.flush()
            }
        }
        if #available(OSX 10.12, *) {
            Timer.scheduledTimer(withTimeInterval: Double.leastNonzeroMagnitude, repeats: false, block: {
                timer in
                self.needsDisplay = true
            })
        } else {
            fatalError("Couldn't create Timer: Too old version of macOS (10.12) required.")
        }
    }
}

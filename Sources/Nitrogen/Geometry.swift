#if os(macOS)
import Cocoa
#elseif os(Linux)
import XCB
#endif

public struct NPoint<T> {
    public var x: T
    public var y: T

    public init (x: T, y: T) {
        self.x = x
        self.y = y
    }
}
public struct NSize<T> {
    public var width: T
    public var height: T

    public init (width: T, height: T) {
        self.width = width
        self.height = height
    }
}
public struct NRect<T> {
    public var size: NSize<T>
    public var origin: NPoint<T>

    public init(origin: NPoint<T>, size: NSize<T>) {
        self.origin = origin
        self.size = size
    }
    public init (x: T, y: T, width: T, height: T) {
        self.origin = NPoint(x: x, y: y)
        self.size = NSize(width: width, height: height)
    }
}
extension NRect where T:Numeric {
    func translated(with frame: NRect<T>) -> NRect<T> {
        return NRect<T>(origin: NPoint<T>(x: self.origin.x, y: frame.size.height - self.origin.y - self.size.height), size: self.size)
    }
}
extension NPoint where T:Numeric {
    func translated(with frame: NRect<T>) -> NPoint<T> {
        return NPoint<T>(x: self.x, y: frame.size.height - self.y)
    }
}
#if os(Linux)
extension xcb_rectangle_t {
    init(_ rect: NRect<Int>) {
        self = xcb_rectangle_t(x: Int16(rect.origin.x), y: Int16(rect.origin.y), width: UInt16(rect.size.width), height: UInt16(rect.size.height))
    } 
}
extension xcb_point_t {
    init(_ point: NPoint<Int>) {
        self = xcb_point_t(x: Int16(point.x), y: Int16(point.y))
    }
}
extension xcb_arc_t {
    init(ellipse rect: NRect<Int>) {
        self = xcb_arc_t(x: Int16(rect.origin.x), y: Int16(rect.origin.y), width: UInt16(rect.size.width), height: UInt16(rect.size.height), angle1: 0 << 6, angle2: 360 << 6)
    }
}
#elseif os(macOS)
extension NSRect {
    init(_ rect: NRect<Int>) {
        self = NSRect(origin: NSPoint(x: CGFloat(rect.origin.x), y: CGFloat(rect.origin.y)), size: NSSize(width: CGFloat(rect.size.width), height: CGFloat(rect.size.height)))
    }
}
extension NSPoint {
    init (_ point: NPoint<Int>) {
        self = NSPoint(x: CGFloat(point.x), y: CGFloat(point.y))
    }
}
#endif

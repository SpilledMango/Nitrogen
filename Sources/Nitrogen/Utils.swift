#if os(Linux)
import XCB
#elseif os(macOS)
import Cocoa
#endif

extension Bool {
    init (_ cInt: CInt) {
        if cInt == 0 {
            self = false
        } else {
            self = true
        }
    }
}

import Foundation

#if os(macOS)
import Cocoa
#endif

public struct NColor {
    public var red: Double
    public var green: Double
    public var blue: Double
    public var alpha: Double

    public init(red: Double, green: Double, blue: Double, alpha: Double = 1.0) {
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
    }
    public init(greyscale: Double, alpha: Double = 1.0) {
        self = NColor(red: greyscale, green: greyscale, blue: greyscale, alpha: alpha)
    }
    public init(hue: Double, saturation: Double, brightness: Double, alpha: Double) {
        guard NColor.isValid(hue: hue) else {
            self.red = 0
            self.green = 0
            self.blue = 0
            self.alpha = 0
            return
        }

        let C = (1 - abs(2 * brightness - 1))
        let H = hue / 60
        let X = C * (1 - abs(H.truncatingRemainder(dividingBy: 2) - 1))

        var components: (Double, Double, Double)

        switch H {
        case 0...1:
            components = (C, X, 0)
        case 1...2:
            components = (X, C, 0)
        case 2...3:
            components = (0, C, X)
        case 3...4:
            components = (0, X, C)
        case 4...5:
            components = (X, 0, C)
        case 5...6:
            components = (C, 0, X)
        default:
            components = (0, 0, 0)
        }
        (self.red, self.green, self.blue) = components
        self.alpha = alpha
    }

    private static func isValid(colorPart part: Double) -> Bool {
        return part <= 1 && part >= 0
    }
    private static func isValid(hue: Double) -> Bool {
        return hue >= 0 && hue < 360
    }

    public static var red: NColor {
        return NColor(red: 1, green: 0, blue: 0)
    }
    public static var green: NColor {
        return NColor(red: 0, green: 1, blue: 0)
    }
    public static var blue: NColor {
        return NColor(red: 0, green: 0, blue: 1)
    }
    public static var white: NColor {
        return NColor(greyscale: 1)
    }
    public static var black: NColor {
        return NColor(greyscale: 0)
    }
    public static var lightGrey: NColor {
        return NColor(greyscale: 0.6)
    }
    public static var darkGrey: NColor {
        return NColor(greyscale: 0.3)
    }
    public static var yellow: NColor {
        return NColor(red: 1, green: 1, blue: 0)
    }
    public static var magenta: NColor {
        return NColor(red: 1, green: 0, blue: 1)
    }
    public static var cyan: NColor {
        return NColor(red: 0, green: 1, blue: 1)
    }
}
#if os(macOS)
extension NSColor {
    public convenience init(_ ncolor: NColor) {
        self.init(red: CGFloat(ncolor.red), green: CGFloat(ncolor.green), blue: CGFloat(ncolor.blue), alpha: CGFloat(ncolor.alpha))
    }
}
#endif

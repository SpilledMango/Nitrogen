#if os(macOS)
import Cocoa
#elseif os(Linux)
import XCB
#endif

#if os(Linux)
extension NColor {
    func toXCBMask() -> (mask: UInt32, values: [UInt32]) {
        let contextMask = XCB_GC_FOREGROUND.rawValue | XCB_GC_GRAPHICS_EXPOSURES.rawValue
            
        let redComponent = (UInt32(self.red * 0xFF) & 0xFF) << 16
        let greenComponent = (UInt32(self.green * 0xFF) & 0xFF) << 8
        let blueComponent = UInt32(self.blue * 0xFF) & 0xFF
        let colorCode = redComponent | greenComponent | blueComponent
        
        return (mask: contextMask, values: [colorCode, 0])
    }
}
#endif

public class NGraphics {
    weak var window: NWindow!
    
    #if os(Linux)
    func xcbContextSet(color: NColor, context: UInt32) {
        var (contextMask, contextValues) = color.toXCBMask()
        xcb_change_gc(self.window.xcbConnection, context, contextMask, &contextValues)
    }
    func emptyWhiteColorContext() -> UInt32 {
        let id = xcb_generate_id(self.window.xcbConnection)
        var (contextMask, contextValues) = NColor.white.toXCBMask()
        xcb_create_gc(self.window.xcbConnection, id, self.window.xcbWindow, contextMask, &contextValues)
        return id
    }
    internal lazy var xcbFillContext: UInt32 = {
        return emptyWhiteColorContext()
    }()
    internal lazy var xcbStrokeContext: UInt32 = {
        return emptyWhiteColorContext()
    }()
    #endif
    
    public var fillColor: NColor {
        didSet {
            #if os(Linux)
            xcbContextSet(color: self.fillColor, context: self.xcbFillContext)
            #endif
        }
    }
    public var strokeColor: NColor {
        didSet {
            #if os(Linux)
            xcbContextSet(color: self.strokeColor, context: self.xcbStrokeContext)
            #endif
        }
    }
    public var backgroundColor = NColor.white
    
    public init() {
        self.fillColor = NColor.white
        self.strokeColor = NColor.white
    }
    
    public func fill(_ rect: NRect<Int>) {
        #if os(Linux)
        var xcbRect = xcb_rectangle_t(rect.translated(with: self.window.localFrame))
        xcb_poly_fill_rectangle(self.window.xcbConnection, self.window.xcbWindow, self.xcbFillContext, 1, &xcbRect)
        #elseif os(macOS)
        if let cgContext = NSGraphicsContext.current?.cgContext {
            cgContext.setFillColor(NSColor(self.fillColor).cgColor)
            cgContext.fill(NSRect(rect))
        }
        #endif
    }
    
    public func drawLine(from p1: NPoint<Int>, to p2: NPoint<Int>) {
        #if os(macOS)
        if let cgContext = NSGraphicsContext.current?.cgContext {
            cgContext.setStrokeColor(NSColor(self.strokeColor).cgColor)
            cgContext.beginPath()
            cgContext.move(to: CGPoint(p1))
            cgContext.addLine(to: CGPoint(p2))
            cgContext.strokePath()
        }
        #elseif os(Linux)
        var points = [xcb_point_t(p1.translated(with: self.window.localFrame)), xcb_point_t(p2.translated(with: self.window.localFrame))]
        xcb_poly_line(self.window.xcbConnection, UInt8(XCB_COORD_MODE_ORIGIN.rawValue), self.window.xcbWindow, self.xcbStrokeContext, UInt32(points.count), &points)
        xcb_flush(self.window.xcbConnection)
        #endif
    }
    public func fillElipse(`in` rect: NRect<Int>) {
        #if os(macOS)
        if let cgContext = NSGraphicsContext.current?.cgContext {
            cgContext.setFillColor(NSColor(self.fillColor).cgColor)
            cgContext.fillEllipse(in: CGRect(rect))
        }
        #elseif os(Linux)
        var arc = xcb_arc_t(ellipse: rect.translated(with: self.window.localFrame))
        xcb_poly_fill_arc(self.window.xcbConnection, self.window.xcbWindow, self.xcbFillContext, 1, &arc)
        #endif
    }
}

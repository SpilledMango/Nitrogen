#if os(Linux)
import XCB
#elseif os(macOS)
import Cocoa
#endif

import Foundation

public class NWindow {
    public typealias Point = NPoint<Int>
    public typealias Size = NSize<Int>
    public typealias Rect = NRect<Int>

    public enum Error: Swift.Error {
        case connectionError
        case nullPointer
        case titleError
    }
    
    public var frame: Rect

    public var localFrame: Rect {
        return Rect(origin: Point(x: 0, y: 0), size: self.frame.size)
    }

    public var title: String
    public var iconLocation: URL? {
        didSet {
            #if os(macOS)
            guard let iconLocation = iconLocation else {
                return
            }
            guard let icon = NSImage(contentsOf: iconLocation) else {
                return
            }
            NSApp.applicationIconImage = icon
            #endif
        }
    }
    
    public let graphics: NGraphics

    var drawLoopBody: (NWindow) -> Void
#if os(Linux)
    internal let xcbConnection: OpaquePointer
    internal var xcbWindow: UInt32
#endif

    public init(frame: Rect, title: String, drawLoop drawLoopBody: @escaping (NWindow) -> Void) throws {
        #if os(Linux)
        // Open the connection to X11.
        self.xcbConnection = xcb_connect(nil, nil)

        if xcb_connection_has_error(self.xcbConnection) > 0 {
            throw Error.connectionError
        }
        guard let screen = xcb_setup_roots_iterator(xcb_get_setup(self.xcbConnection)).data else {
            throw Error.nullPointer
        }

        // Create the window.
        let windowMask = XCB_CW_BACK_PIXEL.rawValue | XCB_CW_EVENT_MASK.rawValue
        var windowValues: [UInt32] = [screen.pointee.white_pixel, XCB_EVENT_MASK_EXPOSURE.rawValue | XCB_EVENT_MASK_KEY_PRESS.rawValue]

        self.xcbWindow = xcb_generate_id(self.xcbConnection)
        
        xcb_create_window(self.xcbConnection, UInt8(XCB_COPY_FROM_PARENT), self.xcbWindow, screen.pointee.root, Int16(frame.origin.x), Int16(frame.origin.y), UInt16(frame.size.width), UInt16(frame.size.height), 1, UInt16(XCB_WINDOW_CLASS_INPUT_OUTPUT.rawValue), screen.pointee.root_visual, windowMask, &windowValues)
      
        // Set the window title.
        guard let titleAsCString = title.cString(using: String.Encoding.utf8) else {
            throw Error.titleError
        }
        xcb_change_property(self.xcbConnection, UInt8(XCB_PROP_MODE_REPLACE.rawValue), self.xcbWindow, XCB_ATOM_WM_NAME.rawValue, XCB_ATOM_STRING.rawValue, 8, UInt32(strlen(titleAsCString)), titleAsCString)
        xcb_flush(self.xcbConnection)

        xcb_map_window(self.xcbConnection, self.xcbWindow)
        xcb_flush(self.xcbConnection)
        #endif
        self.frame = frame
        self.title = title
        self.drawLoopBody = drawLoopBody
        self.graphics = NGraphics()
        #if os(macOS)
        NSApplication.shared.delegate = AppDelegate(window: self)
        NSApp.setActivationPolicy(NSApplication.ActivationPolicy.regular)
        DispatchQueue.main.async {
            NSApp.activate(ignoringOtherApps: true)
        }
        #endif
        self.graphics.window = self
    }
    public func eventLoop() {
        #if os(Linux)
        var done = false

        while !done {

            guard let event = xcb_wait_for_event(self.xcbConnection) else {
                break
            }
            switch Int32(event.pointee.response_type) {
            case XCB_KEY_PRESS:
                break;
            default: break;
            } 
            free(event)
        }
        #elseif os(macOS)
        NSApp.run()
        #endif
    }
    public func drawLoop() {
        #if os(Linux)
        Thread {
            while true {
                self.graphics.fillColor = self.graphics.backgroundColor
                self.graphics.fill(self.localFrame)
                self.drawLoopBody(self)
                xcb_flush(self.xcbConnection)
                Thread.sleep(forTimeInterval: 0.05)
            }
        }.start()
        #endif
    }

    deinit {
#if os(Linux)
        xcb_disconnect(self.xcbConnection)
#endif
    }
}
